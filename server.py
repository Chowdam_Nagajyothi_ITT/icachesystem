import socket
import json

class Server:
   HOST = socket.gethostname()
   PORT = 2282
   server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   server_socket.bind((HOST,PORT))
   server_socket.listen()
   print("server listening")
    
   connection, address = server_socket.accept()
   print("Connection from: " + str(address))
   
   with connection:
       while True:
           received_message = connection.recv(1024)
           decoded_received_message = received_message.decode("utf-8")
           
           print("Client: ",decoded_received_message)
           send_message = input("Server: ")
           connection.send(bytes(send_message, "utf-8"))
           
           if send_message == "exit":
               break
   
    
if __name__ == "__main__":
    Server()
