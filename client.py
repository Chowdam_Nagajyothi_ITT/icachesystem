
import socket
import json

class Server:
   HOST = socket.gethostname()
   PORT = 2282
   client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   client_socket.connect((HOST,PORT))
   
   key = input('Enter the key :')
   value = input('Enter the value :')
   message = {'key':value}
   while True:
       message_sent = input("Client: ")
        # client_socket.send(bytes(message_sent, "utf-8"))
       client_socket.send((json.dumps(message_sent)).encode())
        
       received_message = (client_socket.recv(1024)).decode("utf-8")
       print("Server: ",received_message)
       if message_sent == "exit":
           break
   
    
if __name__ == "__main__":
    Server()
