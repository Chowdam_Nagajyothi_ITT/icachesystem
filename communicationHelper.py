from http import client
import socket

from server import HOST
from server import PORT

def create_client_socket(self):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((HOST,PORT))
    return client_socket

def create_server_socket(self):
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((HOST,PORT))
    server_socket.listen()
    print("server listening")
    return server_socket

def connect_with_client(self, server_socket):
    connection, address = server_socket.accept()
    return print("Connection from: " + str(address))